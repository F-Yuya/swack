<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <style type="text/css">
    </style>
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
    $(function() {
    	  if ( $('#text').val().length == 0 ) {
    	    $('#submit').attr('disabled', 'disabled');
    	  }
    	  $('#text').bind('keydown keyup keypress change', function() {
    	    if ( $(this).val().length > 0 ) {
    	      $('#submit').removeAttr('disabled');
    	    } else {
    	      $('#submit').attr('disabled', 'disabled');
    	    }
    	  });
    	});
    </script>
</head>


<body>

    <div id="container">
    	<form action="InviteMemberList" method="post">
        <div class="header">
            <div class="team-menu">ワークスペース</div>
            <div class="channel-menu"><span class="channel-menu_name"><span class="channel-menu_prefix"><img src="img/sb.png"/></span><c:out value="${receivername}" /></span>
            <c:if test="${OR}">
            			<input type="hidden" name="receiverid" value="${receiverid}">
            			<input type="submit" class="button-OR" value="メンバーの追加">
            </form>
            </c:if>
            </div>
        </div>
    	</form>
        <div class="main">
            <div class="listings">
                <div class="listings_channels">
                <form action="MenberList" method="post">
                <h2 class="listings_header">チャンネル　　　　　<input type="image" class="listings_header_img" src="img/p.png" ></h2>
                </form>
                    <ul class="channel_list">
                    	<c:forEach var="channel" items="${channelList}">
                    	<form method="post" action="IndexServlet">
                    		<input type="hidden" name="receiverid" value="${channel.channelId}">
                    		<c:if test="${channel.channelType == 'on'}" var="flg1"/>
                    		<c:if test="${flg1}">
                    			<li class="channel"><span><span class="prefix"><input type="image" src="img/s.png"/> </span><input type="submit" class="button" name="reciever" value="${channel.channelName}"></span></li>
                    		</c:if>
                    		<c:if test="${!flg1}">
                    			<li class="channel"><span><span class="prefix"><input type="image" src="img/k.png"/> </span><input type="submit" class="button" name="reciever" value="${channel.channelName}"></span></li>
                    		</c:if>
                    	</form>
                    	</c:forEach>
                    </ul>
                    <form action="NotInChannel" method="post">
                	<h2 class="listings_header">チャンネルに参加する<input type="image" class="listings_header_img" src="img/p.png" ></h2>
                	</form>
                </div>

                <div class="listings_direct-messages">
                <form action="TalkMemberList" method="get">
                    <h2 class="listings_header">ダイレクトメッセージ<input type="image" class="listings_header_img" src="img/p.png" ></h2>
                </form>
                    <ul class="channel_list">
                    	<c:forEach var="talk" items="${receiverNameList}" varStatus="status">
                    	 <form method="post" action="IndexServlet">
                    		<input type="hidden" name="receiverid" value="${talkList[status.index].talkId}">
                        	<li class="channel"><span><span class="prefix"><input type="image" src="img/s.png"/> </span><input type="submit" class="button" name="reciever" value="${talk}"></span></li>
                		</form>
                		</c:forEach>
                    </ul>
                </div>
            </div>
            <div class="message-history">
                <c:forEach var="message" items="${messageList}" varStatus="status">
                    <hr>
                	<div class="message"><a class="message_profile-pic"></a><a class="message_username"><c:out value="${senderNameList[status.index]}" /></a><span class="message_timestamp"><c:out value="${message.date}" /></span><span class="message_star"></span><span class="message_content"><c:out value="${message.text}" /></span></div>
                </c:forEach>
            </div>
        </div>
        <c:set var="MS">
        ${"#"}${receivername}${"へのメッセージ"}
        </c:set>
        <div class="footer">
            <div class="user-menu"><input type="image" id="btn" src="img/d.png" class="user-menu_profile-pic"><span class="user-menu_username"><c:out value="${sendername}" /></span><span class="connection_status">ログイン中</span></div>
            <script>
				var btn = document.getElementById('btn');

				btn.addEventListener('click', function() {
				    var result = window.confirm('ログアウトしますか？');

				    if( result ) {
				        console.log('OKがクリックされました');
				        window.location.href = "DeleteSession";
				    }
				    else {
				        console.log('キャンセルがクリックされました');
				    }
				})
			</script>
            <form action="SendMessage" method="post">
    			<input type="hidden" name="receiver" value="${receiverid}">
                <div class="input-box">
                    <input class="input-box_text" type="text" name="text" placeholder="${MS}" id="text"/>
                    <input class="input-box_button" type="submit" value="送信" id="submit"/>
                </div>
            </form>
        </div>
    </div>
</body>
</html>