<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>チャンネル</title>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-toggle.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="js/channel.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="css/bootstrap-select.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/channel.css">
</head>
<body>
	<div class="container form-container">
		<div class="row">
			<div class="col-md-12 channel-form">
				<h3>チャンネルに参加する</h3>
				<p class="input_note_special medium_bottom_margin">チャンネルとはメンバーがコミュニケーションを取る場所です。</p>
				<form action="ChannelInServlet" method="post">
					<div class="form-group">
						<label class="control-label">参加可能なチャンネル一覧</label>
						<select class="form-control selectpicker" name="channelid" required data-live-search="true" data-selected-text-format="count > 1">
							<c:forEach var="channel" items="${channelList}">
							<option disabled="disabled" class="selected" selected="selected" value=""><c:out value="${\"チャンネルを選択してください\"}"/></option>
							<option value="${channel.channelId}"><c:out value="${channel.channelName}" /></option>
							</c:forEach>
						</select>
						<span class="users-note">このチャンネルに追加したい人を選んでください。</span>
					</div>
					<div class="member-form-btn">
					<button id="send" class="btn btn-default">チャンネルに参加する</button>
					</div>
				</form>
				<form action="IndexServlet" method="post">
					<div class="channel-form-btn">
						<button class="btn btn-default">キャンセル</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>