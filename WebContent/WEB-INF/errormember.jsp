<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>チャンネル</title>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-toggle.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="js/channel.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="css/bootstrap-select.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/channel.css">
</head>
<body>
	<div class="container form-container">
		<div class="row">
			<div class="col-md-12 member-form">
				<h3>他のユーザを招待する</h3>
				<h5><font color="red">ユーザーの招待に失敗しました。</font></h5>
				<form action="JoinChannelServlet" method="post">
					<div class="form-group">
						<label class="control-label">招待の送信先:</label> <select class="form-control selectpicker" data-live-search="true" data-selected-text-format="count > 1" multiple>
							<c:forEach var="member" items="${memberList}">
							<option value="${member.id}"><c:out value="${member.name}" /></option>
							</c:forEach>
						</select>
						<span class="users-note">このチャンネルに追加したい人を選んでください。</span>
					</div>
					<div class="member-form-btn">
						<button class="btn btn-default">キャンセル</button>
						<button id="send" class="btn btn-default">招待する</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>