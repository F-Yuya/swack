//	$(function() {
//		$("#container").hide();
//		$("#loading").delay(3000).fadeOut(500);
//		$("#container").delay(5000).fadeIn();
//	});
//	    setInterval(function(){
//	        $('#container').load('Index?p=1');
//	    }, 5000); // 1000ミリ秒間隔で更新
//}
//function load() {
//    $('#container').load('Index'); //初回呼び出し
//	$(function() {
//		$("#container").hide();
//		$("#loading").delay(3000).fadeOut(500);
//		$("#container").delay(5000).fadeIn();
//	});
//    setInterval(function(){
//    	alert(1);
//    	$('#container').load('Index');
//    }, 5000); // 1000ミリ秒間隔で更新
//}
//new Ajax.PeriodicalUpdater('container', // 返り値を挿入するidを指定
//'Index?p=1', // 非同期通信を行いたいURL
//{
//	method : 'get',
//	frequency : 2
//// 何秒毎に通信を行うか
//})

$(window).on('load', function() {
	$("#container").hide();
	$("#loading").delay(400).fadeOut(200);
	$("#container").delay(500).fadeIn();
//  setInterval(function(){
//	alert(1);
//	$('#container').load('Index #message-history');
//}, 2000); // 1000ミリ秒間隔で更新
});

$(function() {
	$(".message-history").scrollTop($(".message-history")[0].scrollHeight);
	$('#message').attr('autofocus', 'autofocus');
	$("input[type=text]").keypress(
			function(ev) {
				if ((ev.which && ev.which === 13)
						|| (ev.keyCode && ev.keyCode === 13)) {
					return false;
				} else {
					return true;
				}
			});
	if ($("#message").val().length == 0) {
		$("#send").css("color", "rgba(44,45,48,.75)");
		$("#send").css("background", "#e8e8e8");
		$("#send").prop("disabled", true);
	}
	$("#message").on("keydown keyup keypress change", function() {
		if ($(this).val().length < 1) {
			$("#send").css("color", "rgba(44,45,48,.75)");
			$("#send").css("background", "#e8e8e8");
			$("#send").prop("disabled", true);
		} else {
			$("#send").css("color", "#ffffff");
			$("#send").css("background", "#008952");
			$("#send").prop("disabled", false);
		}
	});
	$('#message').keydown(function(e) {
		// ctrlキーが押されてる状態か判定
		if (event.ctrlKey) {
			// 押されたキー（e.keyCode）が13（Enter）かそしてテキストエリアに何かが入力されているか判定
			if (e.keyCode === 13 && $(this).val()) {
				// フォームを送信
				$('#sendMessage').submit();
				return false;
			}
		}
	});
});