package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.BelongBean;
import bean.ChannelBean;
import bean.MemberBean;
import dao.ChannelDAO;
import exception.DatabaseException;
import exception.SystemException;

/**
 * Servlet implementation class ChannelInServlet
 */
@WebServlet("/ChannelInServlet")
public class ChannelInServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ChannelInServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ChannelDAO channeldao = new ChannelDAO();
		HttpSession session = request.getSession();
		BelongBean belongBean = new BelongBean();
		int flg = 0;
		MemberBean member = (MemberBean) session.getAttribute("member");

		belongBean.setMemberId(member.getId());
		ChannelBean channel = new ChannelBean();
		channel.setChannelId(request.getParameter("channelid"));
		belongBean.setChannelId(channel.getChannelId());
		try {
			flg = channeldao.belongRegister(belongBean);
		} catch (DatabaseException | SystemException e) {
			e.printStackTrace();
			request.setAttribute("Except", e);
			getServletContext().getRequestDispatcher("/WEB-INF/errormember.jsp").forward(request, response);
			return;
		}
		if (flg == 1) {
			request.setAttribute("id", belongBean.getChannelId());
			getServletContext().getRequestDispatcher("/IndexServlet").forward(request, response);
		} else {
			getServletContext().getRequestDispatcher("/WEB-INF/errormember.jsp").forward(request, response);
		}
	}

}
