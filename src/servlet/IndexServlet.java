package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.ChannelBean;
import bean.MemberBean;
import bean.MessageBean;
import bean.TalkBean;
import dao.ChannelDAO;
import dao.MemberDAO;
import dao.MessageDAO;
import dao.TalkDAO;
import exception.DatabaseException;
import exception.SystemException;

/**
 * Servlet implementation class Index
 */
@WebServlet("/IndexServlet")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("receiverid", request.getAttribute("receiverid"));
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String receiverid = (String) request.getParameter("receiverid");

		ChannelDAO channelDAO = new ChannelDAO();
		TalkDAO talkDAO = new TalkDAO();
		MemberDAO memberDAO = new MemberDAO();
		MessageDAO messageDAO = new MessageDAO();

		ArrayList<MessageBean> messageList = null;
		ArrayList<ChannelBean> channelList = null;
		ArrayList<TalkBean> talkList = null;
		ArrayList<String> receiverNameList = new ArrayList<>();
		ArrayList<String> senderNameList = new ArrayList<>();

		HttpSession session = request.getSession();
		MemberBean member = (MemberBean) session.getAttribute("member");
		if (session.getAttribute("member") == null) {
			Cookie[] cookies = request.getCookies();
			if (cookies != null) {
				System.out.println("kukki0-----------ああああああ");
				for (Cookie cook : cookies) {
					if (cook.getName().equals("memberid")) {
						member.setId(cook.getValue());

					}
					if (cook.getName().equals("membername")) {
						member.setName(cook.getValue());
					}
				}
			}
		}


		String receivername = null;
		if (receiverid == null) {
			receiverid = (String) request.getAttribute("receiverid");
		}
		if (receiverid == null) {
			receiverid = "channel0";
		}


		try {
			channelList = channelDAO.selectChannelList(member.getId());
			talkList = talkDAO.selectTalkList(member.getId());
		} catch (DatabaseException | SystemException e) {
			e.printStackTrace();
			request.setAttribute("Except", e);
			getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
			return;
		}
		for (TalkBean t : talkList) {
			try {
				receiverNameList.add(memberDAO.selectName(t.getReceiverId()));
			} catch (DatabaseException | SystemException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
				e.printStackTrace();
				request.setAttribute("Except", e);
				getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
				return;
			}
		}
		if (receiverid != null) {
			try {
				messageList = messageDAO.selectAll(receiverid); // チャンネルIDかとーくID
			} catch (DatabaseException | SystemException e) {
				e.printStackTrace();
				request.setAttribute("Except", e);
				getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
				return;
			}
		}
		if (receiverid != null) {
			for (MessageBean m : messageList) {
				try {
					senderNameList.add(memberDAO.selectName(m.getSender()));
				} catch (DatabaseException | SystemException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
					e.printStackTrace();
					request.setAttribute("Except", e);
					getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
					return;
				}
			}
		}
		boolean OR = true;
		//受け取り手の名前
		if (receiverid != null) {
			for (int i = 0; i < channelList.size(); i++) {
				if (channelList.get(i).getChannelId().equals(receiverid)) {
					receivername = channelList.get(i).getChannelName();
					OR = true;
					break;
				}
			}
		}
		if (receiverid != null) {
			for (int i = 0; i < talkList.size(); i++) {
				if (talkList.get(i).getTalkId().equals(receiverid) && member.getId().equals(talkList.get(i).getSenderId())) {
					try {
						receivername = memberDAO.selectName(talkList.get(i).getReceiverId());
						OR = false;
						break;
					} catch (DatabaseException | SystemException e) {
						e.printStackTrace();
						request.setAttribute("Except", e);
						getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
					}
				}
			}
		}


		request.setAttribute("messageList", messageList);
		request.setAttribute("channelList", channelList);
		request.setAttribute("talkList", talkList);
		request.setAttribute("senderNameList", senderNameList);
		request.setAttribute("receiverNameList", receiverNameList);
		request.setAttribute("sendername", member.getName());
		request.setAttribute("receiverid", receiverid);
		request.setAttribute("receivername", receivername);
		request.setAttribute("OR", OR);


		getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);

	}

}
