package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.MemberBean;
import dao.ChannelDAO;
import dao.MemberDAO;
import exception.DatabaseException;
import exception.SystemException;

/**
 * Servlet implementation class MenberLsit
 */
@WebServlet("/MenberList")
public class MenberLsit extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenberLsit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MemberDAO memberDAO = new MemberDAO();
		ChannelDAO channeldao = new ChannelDAO();
		ArrayList<MemberBean> memberList = null;
		HttpSession session = request.getSession();
		MemberBean member = (MemberBean) session.getAttribute("member");

		try {
			memberList = memberDAO.selectMemberList(String.valueOf(channeldao.registerSize()));
		} catch (DatabaseException | SystemException e) {
			e.printStackTrace();
			request.setAttribute("Except", e);
			getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
			return;
		}
		for (int i = 0; i < memberList.size(); i++) {
			if (memberList.get(i).getId().equals(member.getId())) {
				memberList.remove(i);
			}
		}
		request.setAttribute("memberList", memberList);
		getServletContext().getRequestDispatcher("/WEB-INF/channel.jsp").forward(request, response);

	}

}

