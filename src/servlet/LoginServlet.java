package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.MemberBean;
import dao.MemberDAO;
import dao.loginDAO;
import exception.DatabaseException;
import exception.SystemException;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		loginDAO logindao = new loginDAO();
		MemberDAO memberDAO = new MemberDAO();
		MemberBean member = new MemberBean();
		member.setId(request.getParameter("id"));
		member.setPass(request.getParameter("pass"));
		try {
			if (logindao.loginCheck(member.getId(), member.getPass())) {
				member.setName(memberDAO.selectName(member.getId()));
				HttpSession session = request.getSession();
				session.setAttribute("member", member);
				Cookie cookMemberId = new Cookie("memberid", member.getId());
				cookMemberId.setMaxAge(7200);
				response.addCookie(cookMemberId);
				Cookie cookMemberName = new Cookie("membername", member.getName());
				cookMemberName.setMaxAge(7200);
				response.addCookie(cookMemberName);
				getServletContext().getRequestDispatcher("/IndexServlet").forward(request, response);
			} else {
				getServletContext().getRequestDispatcher("/loginerror.html").forward(request, response);
			}
		} catch (DatabaseException | SystemException e) {
			e.printStackTrace();
			request.setAttribute("Except", e);
			getServletContext().getRequestDispatcher("/loginerror.html").forward(request, response);
			return;
		}
		//doGet(request, response);
	}

}
