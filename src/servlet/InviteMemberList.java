package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.MemberBean;
import dao.MemberDAO;
import exception.DatabaseException;
import exception.SystemException;

/**
 * Servlet implementation class InviteMemberList
 */
@WebServlet("/InviteMemberList")
public class InviteMemberList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public InviteMemberList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MemberDAO memberDAO = new MemberDAO();
		ArrayList<MemberBean> memberList = null;

		try {
			memberList = memberDAO.selectMemberList(request.getParameter("receiverid"));
		} catch (DatabaseException | SystemException e) {
			e.printStackTrace();
			request.setAttribute("Except", e);
			getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
			return;
		}
		HttpSession session = request.getSession();
		session.setAttribute("channelid", request.getParameter("receiverid"));
		request.setAttribute("memberList", memberList);
		getServletContext().getRequestDispatcher("/WEB-INF/member.jsp").forward(request, response);

	}


}
