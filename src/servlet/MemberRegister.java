package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.BelongBean;
import bean.MemberBean;
import dao.ChannelDAO;
import dao.MemberDAO;
import exception.DatabaseException;
import exception.SystemException;

/**
 * Servlet implementation class MessageRegister
 */
@WebServlet("/MemberRegister")
public class MemberRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MemberRegister() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MemberBean memberBean = new MemberBean();
		MemberDAO memberDAO = new MemberDAO();
		ChannelDAO channeldao = new ChannelDAO();
		memberBean.setId(request.getParameter("id"));
		memberBean.setName(request.getParameter("name"));
		memberBean.setPass(request.getParameter("pass"));
		BelongBean belongbean = new BelongBean();
		belongbean.setChannelId("channel0");
		belongbean.setMemberId(memberBean.getId());
		try {
			if (memberDAO.register(memberBean)) {
				channeldao.belongRegister(belongbean);
				request.setAttribute("id", memberBean.getId());
				HttpSession session = request.getSession();
				session.setAttribute("member", memberBean);
				getServletContext().getRequestDispatcher("/IndexServlet").forward(request, response);
			} else {
				getServletContext().getRequestDispatcher("/registrerror.html").forward(request, response);
			}
		} catch (DatabaseException | SystemException e) {
			e.printStackTrace();
			request.setAttribute("Except", e);
			getServletContext().getRequestDispatcher("/registrerror.html").forward(request, response);
			return;
		}
	}
}
