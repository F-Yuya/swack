package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.MemberBean;
import dao.MemberDAO;
import dao.TalkDAO;
import exception.DatabaseException;
import exception.SystemException;

/**
 * Servlet implementation class MenberLsit
 */
@WebServlet("/TalkMemberList")
public class TalkMemberList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
	public TalkMemberList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MemberDAO memberDAO = new MemberDAO();
		TalkDAO talkldao = new TalkDAO();
		ArrayList<MemberBean> memberList = null;
		String talkId = request.getParameter("talkId");
		if (talkId == null) {
			try {
				talkId = "talk" + String.valueOf(talkldao.TalkIdSet() + 1);
			} catch (DatabaseException | SystemException e) {
				e.printStackTrace();
				request.setAttribute("Except", e);
				getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
				return;
		}
		try {
			memberList = memberDAO.selectTalkMemberList(talkId);
		} catch (DatabaseException | SystemException e) {
			e.printStackTrace();
			request.setAttribute("Except", e);
			getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
			return;
		}

		request.setAttribute("memberList", memberList);
			request.setAttribute("talkId", talkId);
			getServletContext().getRequestDispatcher("/WEB-INF/talk.jsp").forward(request, response); //webinfに変える
		}
	}
}


