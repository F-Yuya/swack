package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.BelongBean;
import bean.ChannelBean;
import dao.ChannelDAO;
import exception.DatabaseException;
import exception.SystemException;

@WebServlet("/JoinChannelServlet")
public class JoinChannelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public JoinChannelServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ChannelDAO channeldao = new ChannelDAO();
		BelongBean belongBean = new BelongBean();
		ChannelBean channelbean = new ChannelBean();
		HttpSession session = request.getSession();
		String[] memberList;
		int flg = 0;
		belongBean.setChannelId((String) session.getAttribute("channelid"));
		memberList = request.getParameterValues("member");
		for (String member : memberList) {
			belongBean.setMemberId(member);
			try {
				flg = channeldao.belongRegister(belongBean);
			} catch (DatabaseException | SystemException e) {
				e.printStackTrace();
				request.setAttribute("Except", e);
				getServletContext().getRequestDispatcher("/WEB-INF/errormember.jsp").forward(request, response);
				return;
			}
		}
		if (flg == 1) {
			request.setAttribute("id", channelbean.getChannelId());
			getServletContext().getRequestDispatcher("/IndexServlet").forward(request, response);
		} else {
			getServletContext().getRequestDispatcher("/WEB-INF/errormember.jsp").forward(request, response);
		}
	}
}
