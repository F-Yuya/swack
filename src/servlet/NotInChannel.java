package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.ChannelBean;
import bean.MemberBean;
import dao.ChannelDAO;
import exception.DatabaseException;
import exception.SystemException;

/**
 * Servlet implementation class NotInChannel
 */
@WebServlet("/NotInChannel")
public class NotInChannel extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NotInChannel() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ChannelDAO channeldao = new ChannelDAO();
		ArrayList<ChannelBean> channelList = null;
		HttpSession session = request.getSession();
		MemberBean member = (MemberBean) session.getAttribute("member");

		try {
			channelList = channeldao.notInChannelList(member.getId());
		} catch (DatabaseException | SystemException e) {
			e.printStackTrace();
			request.setAttribute("Except", e);
			getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
			return;
		}
		request.setAttribute("channelList", channelList);
		getServletContext().getRequestDispatcher("/WEB-INF/channelin.jsp").forward(request, response);

	}

}
