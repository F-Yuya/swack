package servlet;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.MemberBean;
import bean.MessageBean;
import bean.TalkBean;
import dao.MemberDAO;
import dao.MessageDAO;
import dao.TalkDAO;
import exception.DatabaseException;
import exception.SystemException;

/**
 * Servlet implementation class SendMessage
 */
@WebServlet("/SendMessage")
public class SendMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendMessage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("receiverid", request.getAttribute("receiverid"));
		getServletContext().getRequestDispatcher("/IndexServlet").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MessageBean messageBean = new MessageBean();
		TalkBean talkBean = new TalkBean();
		String receiverid = request.getParameter("receiver");
		System.out.println(receiverid);
		try {
			HttpSession session = request.getSession();
			MemberBean member = (MemberBean) session.getAttribute("member");
			messageBean.setSender(member.getId());
			messageBean.setReceiver(request.getParameter("receiver"));
			messageBean.setText(request.getParameter("text"));
			LocalDateTime d = LocalDateTime.now();
			DateTimeFormatter df1 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			String s = df1.format(d);
			messageBean.setDate(s);
			MemberDAO memberDAO = new MemberDAO();

			if (receiverid.substring(0, 4).equals("talk")) {
				talkBean.setTalkId(request.getParameter("receiver"));
				talkBean.setDelFlg("0");

				try {
					talkBean.setSenderId(memberDAO.selectTalkMember(request.getParameter("receiver")));
				} catch (DatabaseException | SystemException e) {
					e.printStackTrace();
					request.setAttribute("Except", e);
					getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
					return;
				}
				talkBean.setReceiverId(member.getId());
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			request.setAttribute("Except", e);
			getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
			return;
		}


		MessageDAO messageDAO = new MessageDAO();
		TalkDAO talkDAO = new TalkDAO();
		try {
			messageDAO.register(messageBean);
			if (receiverid.substring(0, 4).equals("talk")) {
				if (!talkDAO.selectTalk2(talkBean)) {
					talkDAO.talkRegister(talkBean);
				}
			}
		} catch (DatabaseException | SystemException e) {
			e.printStackTrace();
			request.setAttribute("Except", e);
			getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(request, response);
			return;
		}

		request.setAttribute("receiverid", request.getParameter("receiver"));
		//		response.sendRedirect("http://localhost:8080/Swack/SendMessage");
		getServletContext().getRequestDispatcher("/IndexServlet").forward(request, response);
	}

}
