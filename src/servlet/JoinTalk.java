package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.MemberBean;
import bean.TalkBean;
import dao.TalkDAO;
import exception.DatabaseException;
import exception.SystemException;

/**
 * Servlet implementation class JoinTalk
 */
@WebServlet("/JoinTalk")
public class JoinTalk extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public JoinTalk() {
        super();
        // TODO Auto-generated constructor stub
    }
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		MemberBean memberBean = (MemberBean) session.getAttribute("member");
		TalkDAO talkdao = new TalkDAO();
		TalkBean talkBean = new TalkBean();
		String[] memberList;
		talkBean.setTalkId(request.getParameter("talkId"));
		talkBean.setSenderId(request.getParameter("id"));
		try {
			memberList = request.getParameterValues("member");
			for (String member : memberList) {
				int flg = 0;
				talkBean.setSenderId(memberBean.getId());
				talkBean.setReceiverId(member);
				talkBean.setDelFlg("1");
				if (!talkdao.selectTalk3(talkBean)) {
					TalkBean checkTalkBean = new TalkBean();
					checkTalkBean.setTalkId(talkBean.getTalkId());
					checkTalkBean.setSenderId(talkBean.getReceiverId());
					checkTalkBean.setReceiverId(talkBean.getSenderId());
					checkTalkBean.setDelFlg("1");
					if (!talkdao.selectTalk3(checkTalkBean)) {
						if (talkdao.selectTalk(talkBean)) {
							flg = talkdao.talkDeleteFlgSet(talkBean);
						} else {
							flg = talkdao.talkRegister(talkBean);
						}
						if (flg == 0) {
							getServletContext().getRequestDispatcher("/WEB-INF/errortalk.jsp").forward(request, response);
						}
					}
				}
				talkBean.setTalkId("talk" + String.valueOf(Integer.parseInt(talkBean.getTalkId())) + 1);
			}
			request.setAttribute("id", talkBean.getSenderId());
			getServletContext().getRequestDispatcher("/IndexServlet").forward(request, response);

		} catch (DatabaseException | SystemException e) {
			e.printStackTrace();
			request.setAttribute("Except", e);
			getServletContext().getRequestDispatcher("/WEB-INF/errortalk.jsp").forward(request, response);
			return;
		} catch (NumberFormatException e) {
			request.setAttribute("id", talkBean.getSenderId());
			getServletContext().getRequestDispatcher("/IndexServlet").forward(request, response);
		}
	}
}
