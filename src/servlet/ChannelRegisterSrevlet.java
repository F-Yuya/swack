package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.BelongBean;
import bean.ChannelBean;
import bean.MemberBean;
import dao.ChannelDAO;
import exception.DatabaseException;
import exception.SystemException;

/**
 * Servlet implementation class JoinChannelSrevlet
 */
@WebServlet("/ChannelRegisterServlet")
public class ChannelRegisterSrevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChannelRegisterSrevlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ChannelDAO channeldao = new ChannelDAO();
		BelongBean belongBean = new BelongBean();
		ChannelBean channelbean = new ChannelBean();
		HttpSession session = request.getSession();
		MemberBean memberBean = (MemberBean) session.getAttribute("member");
		String[] memberList;
		try {
			channelbean.setChannelId("channel" + String.valueOf(channeldao.registerSize()));
		} catch (DatabaseException | SystemException e) {
			e.printStackTrace();
			request.setAttribute("Except", e);
			getServletContext().getRequestDispatcher("/WEB-INF/errorchannel.jsp").forward(request, response);
			return;
		}
		channelbean.setChannelName(request.getParameter("name"));
		if (request.getParameter("type") == null) {
			channelbean.setChannelType("off");
		} else {
			channelbean.setChannelType(request.getParameter("type"));
		}
		try {
			int flg = channeldao.channelRegister(channelbean);
			belongBean.setChannelId(channelbean.getChannelId());
			memberList = request.getParameterValues("member");
			for (String member : memberList) {
				belongBean.setMemberId(member);
				channeldao.belongRegister(belongBean);
			}
			belongBean.setMemberId(memberBean.getId());
			channeldao.belongRegister(belongBean);
			if (flg == 1) {
				request.setAttribute("id", channelbean.getChannelId());
				getServletContext().getRequestDispatcher("/IndexServlet").forward(request, response);
			} else {
				getServletContext().getRequestDispatcher("/WEB-INF/errorchannel.jsp").forward(request, response);
			}
		} catch (DatabaseException | SystemException e) {
			e.printStackTrace();
			request.setAttribute("Except", e);
			getServletContext().getRequestDispatcher("/WEB-INF/errorchannel.jsp").forward(request, response);
			return;
		}
	}
}
