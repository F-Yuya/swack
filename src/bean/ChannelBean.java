package bean;

import java.io.Serializable;

public class ChannelBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String channelId;
	private String channelName;
	private String channelType;

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}
}
