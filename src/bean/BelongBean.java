package bean;

import java.io.Serializable;

public class BelongBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String channelId;
	private String memberId;

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

}
