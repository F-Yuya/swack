package bean;

import java.io.Serializable;

public class MemberBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String name;
	private String pass;


	public MemberBean() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getPass() {
		return pass;
	}

}
