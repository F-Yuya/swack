package dao;

import static parameter.ExceptionParameters.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import exception.DatabaseException;
import exception.SystemException;

public class loginDAO extends DAOBase {
	public boolean loginCheck(String id, String pass) throws DatabaseException, SystemException {
		boolean flg;
		this.open();
		ResultSet rs;
		String sql = "SELECT * FROM member where id = ? AND pass = ?";
		try (PreparedStatement pst = conn.prepareStatement(sql)) {
			pst.setString(1, id);
			pst.setString(2, pass);
			rs = pst.executeQuery();
			flg = rs.next();
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return flg;

	}
}
