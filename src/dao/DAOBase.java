package dao;

import static parameter.DAOParameters.*;
import static parameter.ExceptionParameters.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import exception.DatabaseException;
import exception.SystemException;

public class DAOBase {
	Connection conn;

	protected void open() throws DatabaseException, SystemException {
		try {
			Class.forName(DRIVER_NAME);
		} catch (ClassNotFoundException e) {
			throw new SystemException(SYSTEM_EXCEPTION_MESSAGE, e);
		}
		try {
			conn = DriverManager.getConnection(CONNECT_STRING, USERID, PASSWORD);
			conn.setAutoCommit(false);

		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_CONNECTION_EXCEPTION_MESSAGE, e);
		}
	}

	protected void close() throws DatabaseException {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_CLOSE_EXCEPTION_MESSAGE, e);
		}
	}

}
