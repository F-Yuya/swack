package dao;

import static parameter.ExceptionParameters.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import bean.BelongBean;
import bean.ChannelBean;
import exception.DatabaseException;
import exception.SystemException;

public class ChannelDAO extends DAOBase {
	public ArrayList<ChannelBean> selectChannelList(String memberId) throws DatabaseException, SystemException {
		ArrayList<ChannelBean> channelList = new ArrayList<>();

		this.open();
		ResultSet rs;
		String sql = "SELECT c.channelid,c.channelname,c.Type FROM belongs b, channel c "
				+ "where c.channelid = b.channelid AND b.memberid = ?";
		try (PreparedStatement pst = conn.prepareStatement(sql)) {
			pst.setString(1, memberId);
			rs = pst.executeQuery();
			while (rs.next()) {
				ChannelBean channelBean = new ChannelBean();
				channelBean.setChannelId(rs.getString("channelid"));
				channelBean.setChannelName(rs.getString("channelname"));
				channelBean.setChannelType(rs.getString("Type"));
				channelList.add(channelBean);
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return channelList;
	}

	public int channelRegister(ChannelBean channelBean) throws DatabaseException, SystemException {

		this.open();
		int cnt = 0;
		String sql = "INSERT INTO channel VALUES(?, ?, ?)";
		try (PreparedStatement pst = conn.prepareStatement(sql);) {
			pst.setString(1, channelBean.getChannelId());
			pst.setString(2, channelBean.getChannelName());
			pst.setString(3, channelBean.getChannelType());
			cnt = pst.executeUpdate();
			if (cnt == 1) {
				conn.commit();
			} else {
				conn.rollback();
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return cnt;
	}

	public int belongRegister(BelongBean belongBean) throws DatabaseException, SystemException {

		this.open();
		int cnt = 0;
		String sql = "INSERT INTO belongs VALUES(?, ?)";
		try (PreparedStatement pst = conn.prepareStatement(sql);) {
			pst.setString(1, belongBean.getChannelId());
			pst.setString(2, belongBean.getMemberId());
			cnt = pst.executeUpdate();
			if (cnt == 1) {
				conn.commit();
			} else {
				conn.rollback();
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return cnt;
	}


	public int registerSize() throws DatabaseException, SystemException {
		int size = 0;
		this.open();
		ResultSet rs;
		String sql = "SELECT * FROM channel";
		try (PreparedStatement pst = conn.prepareStatement(sql);) {
			rs = pst.executeQuery();
			while (rs.next()) {
				size++;
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return size;
	}

	public ArrayList<ChannelBean> notInChannelList(String memberId) throws DatabaseException, SystemException {
		ArrayList<ChannelBean> channelList = new ArrayList<>();
		this.open();
		ResultSet rs;
		String sql = "SELECT channelid,channelname,type FROM  channel " +
				"WHERE channelid NOT IN ( " +
				"SELECT c.CHANNELID  " +
				"FROM belongs b ,channel c " +
				"WHERE b.CHANNELID = c.CHANNELID  " +
				"AND MEMBERID = ? " +
				")" +
				"AND type = 'on' ";
		try (PreparedStatement pst = conn.prepareStatement(sql)) {
			pst.setString(1, memberId);
			rs = pst.executeQuery();
			while (rs.next()) {
				ChannelBean channelBean = new ChannelBean();
				channelBean.setChannelId(rs.getString("channelid"));
				channelBean.setChannelName(rs.getString("channelname"));
				channelBean.setChannelType(rs.getString("type"));
				channelList.add(channelBean);
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return channelList;
	}
}
