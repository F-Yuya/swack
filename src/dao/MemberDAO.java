package dao;

import static parameter.ExceptionParameters.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import bean.MemberBean;
import exception.DatabaseException;
import exception.SystemException;

public class MemberDAO extends DAOBase {
	public String selectName(String id) throws DatabaseException, SystemException {
		String name = null;
		this.open();
		ResultSet rs;
		String sql = "SELECT name FROM member where id = ?";
		try (PreparedStatement pst = conn.prepareStatement(sql)) {
			pst.setString(1, id);
			rs = pst.executeQuery();
			if (rs.next()) {
				name = rs.getString("NAME");
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return name;

	}

	public boolean register(MemberBean memberBean) throws DatabaseException, SystemException {
		boolean flg;
		this.open();
		int cnt = 0;
		String sql = "INSERT INTO MEMBER VALUES(?, ?, ?)";
		try (PreparedStatement pst = conn.prepareStatement(sql);) {
			pst.setString(1, memberBean.getId());
			pst.setString(2, memberBean.getName());
			pst.setString(3, memberBean.getPass());
			cnt = pst.executeUpdate();
			if (cnt == 1) {
				conn.commit();
				flg = true;
			} else {
				conn.rollback();
				flg = false;
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return flg;
	}

	public ArrayList<MemberBean> selectMemberList(String channelId) throws DatabaseException, SystemException {
		ArrayList<MemberBean> memberList = new ArrayList<>();

		this.open();
		ResultSet rs;
		String sql = "SELECT id,name,pass FROM  member " +
				"WHERE id NOT IN ( " +
				"SELECT id " +
				"FROM member ,belongs  " +
				"WHERE id = MEMBERID " +
				"AND CHANNELID = ? " +
				")";
		try (PreparedStatement pst = conn.prepareStatement(sql)) {
			pst.setString(1, channelId);
			rs = pst.executeQuery();
			while (rs.next()) {
				MemberBean memberBean = new MemberBean();
				memberBean.setId(rs.getString("id"));
				memberBean.setName(rs.getString("name"));
				memberBean.setPass(rs.getString("pass"));
				memberList.add(memberBean);
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return memberList;
	}

	public ArrayList<MemberBean> selectTalkMemberList(String talkId) throws DatabaseException, SystemException {
		ArrayList<MemberBean> memberList = new ArrayList<>();

		this.open();
		ResultSet rs;
		String sql = "SELECT id,name,pass FROM  member " +
				"WHERE id NOT IN ( " +
				"SELECT DISTINCT id " +
				"FROM member ,talk  " +
				"WHERE talkid = ? " +
				"AND id = senderid " +
				")";
		try (PreparedStatement pst = conn.prepareStatement(sql)) {
			pst.setString(1, talkId);
			rs = pst.executeQuery();
			while (rs.next()) {
				MemberBean memberBean = new MemberBean();
				memberBean.setId(rs.getString("id"));
				memberBean.setName(rs.getString("name"));
				memberBean.setPass(rs.getString("pass"));
				memberList.add(memberBean);
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return memberList;
	}

	public String selectTalkMember(String talkId) throws DatabaseException, SystemException {
		String receiver = null;
		this.open();
		ResultSet rs;
		String sql = "SELECT id FROM  member " +
				"WHERE id IN ( " +
				"SELECT DISTINCT id " +
				"FROM member ,talk  " +
				"WHERE talkid = ? " +
				"AND id = receiverid " +
				")";
		try (PreparedStatement pst = conn.prepareStatement(sql)) {
			pst.setString(1, talkId);
			rs = pst.executeQuery();
			while (rs.next()) {
				receiver = rs.getString("id");
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return receiver;
	}
}
