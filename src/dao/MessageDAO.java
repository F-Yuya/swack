package dao;

import static parameter.ExceptionParameters.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import bean.MessageBean;
import exception.DatabaseException;
import exception.SystemException;

public class MessageDAO extends DAOBase {
	public ArrayList<MessageBean> selectAll(String receiverId) throws DatabaseException, SystemException {
		ArrayList<MessageBean> messageList = new ArrayList<>();
		final String DATE_PATTERN ="yyyy/MM/dd HH:mm:ss";

		this.open();
		ResultSet rs;
		String sql = "SELECT * FROM message where receiverid = ? ORDER BY DATUM";
		try (PreparedStatement pst = conn.prepareStatement(sql)) {
			pst.setString(1, receiverId);
			rs = pst.executeQuery();
			while (rs.next()) {
				MessageBean messageBean = new MessageBean();
				messageBean.setSender(rs.getString("senderid"));
				messageBean.setReceiver(rs.getString("receiverid"));
				messageBean.setText(rs.getString("text"));
				messageBean.setDate(new SimpleDateFormat(DATE_PATTERN).format(rs.getDate("datum")));
				messageList.add(messageBean);
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return messageList;
	}

	public boolean register(MessageBean messageBean) throws DatabaseException, SystemException {
		boolean flg;
		this.open();
		int cnt = 0;
		String sql = "INSERT INTO message(senderid,receiverid,text) VALUES(?, ?, ?)";
		try (PreparedStatement pst = conn.prepareStatement(sql);) {
			pst.setString(1, messageBean.getSender());
			pst.setString(2, messageBean.getReceiver());
			pst.setString(3, messageBean.getText());
			cnt = pst.executeUpdate();
			if (cnt == 1) {
				conn.commit();
				flg = true;
			} else {
				conn.rollback();
				flg = false;
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return flg;
	}
}
