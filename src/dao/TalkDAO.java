package dao;

import static parameter.ExceptionParameters.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import bean.TalkBean;
import exception.DatabaseException;
import exception.SystemException;

public class TalkDAO extends DAOBase {
	public ArrayList<TalkBean> selectTalkList(String senderId) throws DatabaseException, SystemException {
		ArrayList<TalkBean> talkList = new ArrayList<>();
		this.open();
		ResultSet rs;
		String sql = "SELECT * FROM talk where senderid = ? AND delflg = 0";
		try (PreparedStatement pst = conn.prepareStatement(sql)) {
			pst.setString(1, senderId);
			rs = pst.executeQuery();
			while (rs.next()) {
				TalkBean talkBean = new TalkBean();
				talkBean.setTalkId(rs.getString("talkid"));
				talkBean.setSenderId(rs.getString("senderid"));
				talkBean.setReceiverId(rs.getString("receiverid"));
				talkList.add(talkBean);
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return talkList;
	}

	public int talkRegister(TalkBean talkBean) throws DatabaseException, SystemException {
		this.open();
		int cnt = 0;
		String sql = "INSERT INTO talk VALUES(?, ?, ?,0)";
		try (PreparedStatement pst = conn.prepareStatement(sql);) {
			pst.setString(1, talkBean.getTalkId());
			pst.setString(2, talkBean.getSenderId());
			pst.setString(3, talkBean.getReceiverId());
			cnt = pst.executeUpdate();
			if (cnt == 1) {
				conn.commit();
			} else {
				conn.rollback();
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return cnt;
	}

	public int talkDelete(TalkBean talkBean) throws DatabaseException, SystemException {

		this.open();
		int cnt = 0;
		String sql = "UPDATE talk SET delflg = 1 WHERE talkid = ? AND senderid = ?";
		try (PreparedStatement pst = conn.prepareStatement(sql);) {
			pst.setString(1, talkBean.getTalkId());
			pst.setString(2, talkBean.getSenderId());
			cnt = pst.executeUpdate();
			if (cnt == 1) {
				conn.commit();
			} else {
				conn.rollback();
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return cnt;
	}

	public boolean selectTalk(String senderId, String receiverId) throws DatabaseException, SystemException {
		this.open();
		ResultSet rs = null;
		boolean flg = false;
		String sql = "SELECT * FROM talk where senderid = ? AND receiverid = ? AND delflg = 1";
		try (PreparedStatement pst = conn.prepareStatement(sql)) {
			while (rs.next()) {
				pst.setString(1, senderId);
				pst.setString(2, receiverId);
				rs = pst.executeQuery();
				flg = rs.next();
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return flg;
	}

	public int TalkIdSet() throws DatabaseException, SystemException {
		this.open();
		int size = 0;
		ResultSet rs = null;
		String sql = "SELECT DISTINCT talkid FROM talk ";
		try (PreparedStatement pst = conn.prepareStatement(sql)) {
			rs = pst.executeQuery();
			while (rs.next()) {
				size++;
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return size;
	}

	public int talkDeleteFlgSet(TalkBean talkBean) throws DatabaseException, SystemException {

		this.open();
		int cnt = 0;
		String sql = "UPDATE talk SET delflg = 0 WHERE talkid = ? AND senderid = ?";
		try (PreparedStatement pst = conn.prepareStatement(sql);) {
			pst.setString(1, talkBean.getTalkId());
			pst.setString(2, talkBean.getSenderId());
			cnt = pst.executeUpdate();
			if (cnt == 1) {
				conn.commit();
			} else {
				conn.rollback();
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return cnt;
	}

	public boolean selectTalk(TalkBean talkBean) throws DatabaseException, SystemException {
		this.open();
		boolean flg = false;
		ResultSet rs;
		String sql = "SELECT * FROM talk where talkid = ? AND senderid = ?  AND delflg = 1";
		try (PreparedStatement pst = conn.prepareStatement(sql)) {
			pst.setString(1, talkBean.getTalkId());
			pst.setString(2, talkBean.getSenderId());
			rs = pst.executeQuery();
			while (rs.next()) {
				flg = true;
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return flg;
	}

	public boolean selectTalk2(TalkBean talkBean) throws DatabaseException, SystemException {
		this.open();
		boolean flg = false;
		ResultSet rs;
		String sql = "SELECT * FROM talk where talkid = ? AND senderid = ?";
		try (PreparedStatement pst = conn.prepareStatement(sql)) {
			pst.setString(1, talkBean.getTalkId());
			pst.setString(2, talkBean.getSenderId());
			rs = pst.executeQuery();
			while (rs.next()) {
				flg = true;
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return flg;
	}

	public boolean selectTalk3(TalkBean talkBean) throws DatabaseException, SystemException {
		this.open();
		boolean flg = false;
		ResultSet rs;
		String sql = "SELECT talkid FROM talk WHERE senderid = ? AND receiverid = ?";
		try (PreparedStatement pst = conn.prepareStatement(sql)) {
			pst.setString(1, talkBean.getSenderId());
			pst.setString(2, talkBean.getReceiverId());
			rs = pst.executeQuery();
			while (rs.next()) {
				flg = true;
			}
		} catch (SQLException e) {
			throw new DatabaseException(DATABASE_PROCESSING_EXCEPTION_MESSAGE, e);
		} finally {
			this.close();
		}
		return flg;
	}
}
